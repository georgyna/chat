const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const cors = require('cors');

const app = express();
const port = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.get('/api/messages', (req, res) => {
    const rawdata = fs.readFileSync('./resources/api/messages.json');
    const messageList = JSON.parse(rawdata);
    res.send(messageList);
});

app.post('/api/login', (req, res) => {
    const rawData = fs.readFileSync('./resources/api/users.json');
    const users = JSON.parse(rawData);
    const [ authUser ] = users.filter(({ login, password }) => req.body.login === login && req.body.password === password);
    if (authUser) {
        res.send(authUser);
    } else {
        res.status(404).send({ message: 'User not exists.' });
    }
});

app.listen(port, () => console.log(`Listening on port ${port}`));