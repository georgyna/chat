export const readableMonthDate = (date: string) => {
    const [day, month] = date.split(' ');
    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return `${day} ${months[Number(month)]}`
};

export default (timestamp: string): string => {
    const options = { 
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric', 
        second: 'numeric' 
    };
    return new Date(timestamp).toLocaleDateString('en-US', options);
}
