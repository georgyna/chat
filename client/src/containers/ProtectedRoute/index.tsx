import React from 'react';
import { Redirect } from 'react-router-dom'

interface IProtectedRouteProps {
    component: any;
    path?: string;
}

const ProtectedRoute = (props: IProtectedRouteProps) => {
    const Component = props.component;
    const isAuth = true;

    return isAuth ? <Component /> : (<Redirect to={{ pathname: '/login' }} />);
};

export default ProtectedRoute;