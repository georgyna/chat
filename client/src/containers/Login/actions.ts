import { LOGIN } from "./actionTypes";

export const login = (userData: { login: string, password: string }) => ({
    type: LOGIN,
    payload: userData
});