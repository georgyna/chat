import axios from 'axios';
import api from '../../shared/config/api.json';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { SET_USER, LOGIN } from './actionTypes';

export function* login(action: any) {
    try {
        const response = yield call(() => axios.post(`${api.url}/api/login`, action.payload));
        const user = response.data;
        yield put({ type: SET_USER, user });
    } catch (error) {
        console.log('login error:', error.message)
    }
}

function* watchLogin() {
    yield takeEvery(LOGIN, login)
}

export default function* loginSagas() {
    yield all([
        watchLogin()
    ])
};
