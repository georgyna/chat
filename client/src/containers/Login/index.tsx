import React, { useState } from 'react';
import { Button, Form } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { login } from './actions';
import { bindActionCreators } from 'redux';
import { useHistory } from "react-router-dom";

const Login = (props: any) => {
    console.log(props.user);
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');

    const history = useHistory();

    const submitLogin = () => {
        props.login({ login: login, password: password });
    }

    return (
        <Form style={{ width: '300px', margin: '100px auto' }}>
            <Form.Field>
                <label>Login</label>
                <input
                    placeholder='Login'
                    value={login}
                    onChange={ev => setLogin(ev.target.value)}
                />
            </Form.Field>
            <Form.Field>
                <label>Password</label>
                <input
                    placeholder='Password'
                    value={password}
                    onChange={ev => setPassword(ev.target.value)}
                />
            </Form.Field>
            <Button type='submit' onClick={() => submitLogin()}>Submit</Button>
        </Form>
    )
};

const mapStateToProps = (rootState: any) => ({
    user: rootState.user
});

const actions = { login };

const mapDispatchToProps = (dispatch: any) => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Login);