import { call, put, takeEvery, all } from 'redux-saga/effects';
import { LOAD_MESSAGES, SET_MESSAGES } from './actionTypes';
import { IMessage } from '../../interfaces/message.interfaces';
import axios from 'axios';
import api from '../../shared/config/api.json';

export function* loadMessages() {
    try {
        const response = yield call(axios.get, `${api.url}/api/messages`);
        const messages = response.data.sort((a: IMessage, b: IMessage) =>
            new Date(a.createdAt).valueOf() - new Date(b.createdAt).valueOf());
        yield put({ type: SET_MESSAGES, messages });
    } catch (error) {
        console.log('loadMessages error:', error.message)
    }
}

function* watchLoadMessages() {
    yield takeEvery(LOAD_MESSAGES, loadMessages)
}

export default function* chatSagas() {
    yield all([
        watchLoadMessages()
    ])
};
