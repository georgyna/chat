import { ADD_MESSAGE, DELETE_MESSAGE, EDIT_MESSAGE, LOAD_MESSAGES } from './actionTypes';
import { IMessage } from '../../interfaces/message.interfaces';

export const addMessage = (message: IMessage) => ({
    type: ADD_MESSAGE,
    message
});

export const deleteMessage = (id: string) => ({
    type: DELETE_MESSAGE,
    id
});

export const editMessage = (id: string, messageText: string) => ({
    type: EDIT_MESSAGE,
    id,
    messageText
});

export const loadMessages = () => ({
    type: LOAD_MESSAGES
});