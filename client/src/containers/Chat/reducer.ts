import {
  SET_MESSAGES, ADD_MESSAGE, DELETE_MESSAGE, EDIT_MESSAGE
} from './actionTypes';
import { IMessage } from '../../interfaces/message.interfaces';

export default (state: { messages: IMessage[] } = { messages: [] }, action: any) => {
  switch (action.type) {
    case SET_MESSAGES:
      return {
        ...state,
        messages: action.messages
      };
    case ADD_MESSAGE:
      return {
        ...state,
        messages: [...state.messages, action.message]
      };
    case DELETE_MESSAGE:
      return {
        ...state,
        messages: state.messages.filter(({ id }) => id !== action.id)
      };
    case EDIT_MESSAGE:
      return {
        ...state,
        messages: [
          ...state.messages.map((message) => message.id === action.id
            ? ({ ...message, text: action.messageText })
            : message)]
      };
    default:
      return state;
  }
};
