import React, { useEffect } from 'react';
import Header from '../../components/Header';
import MessageList from '../../components/MessageList';
import { Container, Dimmer, Loader, Icon } from 'semantic-ui-react';
import MessageInput from '../../components/MessageInput';
import { connect } from 'react-redux';
import { loadMessages, addMessage, deleteMessage, editMessage } from './actions';
import { bindActionCreators } from 'redux';
import styles from './styles.module.scss';

const Chat = ({ messages, loadMessages, addMessage, deleteMessage, editMessage }: any) => {
    useEffect(() => { loadMessages(); }, []);

    const messagesList = messages
        ? (<Container>
            <Header messages={messages}></Header>
            <MessageList messages={messages} del={deleteMessage} edit={editMessage}></MessageList>
            <MessageInput addMessage={addMessage}></MessageInput>
        </Container>)
        : (<Dimmer active inverted>
            <Loader inverted>Loading</Loader>
        </Dimmer>);

    return (
        <div>
            <div className={styles.ChatLogo}><Icon name="chat" />Chat</div>
            <div className={styles.ChatContainer}>{messagesList}</div>
        </div>
    );
};

const mapStateToProps = (rootState: any) => {
    return ({
        messages: rootState.messages
    })
};

const actions = { loadMessages, addMessage, deleteMessage, editMessage };

const mapDispatchToProps = (dispatch: any) => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Chat);