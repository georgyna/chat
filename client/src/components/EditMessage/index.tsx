import React, { useState } from 'react';
import { Modal, Button, Icon, TextArea } from 'semantic-ui-react';
import { IMessage } from '../../interfaces/message.interfaces';

export interface IEditMessageProps {
    message: IMessage;
    edit: Function;
}

const EditMessage = ({ message: { text, id }, edit }: IEditMessageProps) => {
    const [msgText, setMsgText] = useState(text);
    const editMessage = () => {
        edit(id, msgText);
    }

    return (
        <Modal trigger={<span>Edit</span>}>
            <Modal.Header>Edit Message</Modal.Header>
            <Modal.Content image>
                <TextArea
                    rows={5}
                    style={{ width: '100%' }}
                    value={msgText}
                    onChange={ev => setMsgText((ev.target as HTMLTextAreaElement).value)}
                ></TextArea>
            </Modal.Content>
            <Modal.Actions>
                <Button negative>
                    <Icon name='remove' /> Cancel
                </Button>
                <Button positive onClick={editMessage}>
                    <Icon name='checkmark' /> Ok
                </Button>
            </Modal.Actions>
        </Modal>
    )
};

export default EditMessage;
