import React, { useState } from 'react';
import { Form, TextArea, Button } from 'semantic-ui-react';
import styles from './styles.module.scss';
import { userId } from '../../constants/currentUser';
import { IMessage } from '../../interfaces/message.interfaces';

export interface IMessageInputProps {
    addMessage: Function
};

const MessageInput = ({ addMessage }: IMessageInputProps) => {
    const [messageText, setMessageText] = useState('');
    const handleSendMessage = () => {
        const newMessage: IMessage = { 
            text: messageText,
            user: 'Me',
            id: `msg${Math.floor((Math.random() * 1000) + 1)}`, 
            createdAt: new Date().toISOString(), 
            userId, 
            avatar: '', 
            editedAt: '' 
        };
        addMessage(newMessage);
        setMessageText('');
    };

    return (
        <Form className={styles.MessageForm} onSubmit={handleSendMessage}>
            <TextArea
                rows={3}
                placeholder="Type your message here.."
                value={messageText}
                onChange={ev => setMessageText((ev.target as HTMLTextAreaElement).value)}
            />
            <Button className={styles.MessageFormBtn}>Send</Button>
        </Form>
    )
};

export default MessageInput;
