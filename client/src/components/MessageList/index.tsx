import React from 'react';
import { Comment, Header, Container } from 'semantic-ui-react'
import EditMessage from '../EditMessage';
import { IMessage } from '../../interfaces/message.interfaces';
import convertDate, { readableMonthDate } from '../../utils/convertDate';
import { userId } from '../../constants/currentUser';

export interface IMessageListProps {
  messages: IMessage[];
  del: Function;
  edit: Function;
}

const MessageList = ({ messages, del, edit }: IMessageListProps) => {
  const messagesDates = [...new Set(messages.map(({ createdAt }) =>
    `${new Date(createdAt).getDate()} ${new Date(createdAt).getMonth()}`
  ))];
  return (<Container>
    {messagesDates.map(date => (
      <div key={date}>
        <Header as='h3' dividing>
          {readableMonthDate(date)}
        </Header>
        {
          messages.filter(({ createdAt }) => `${new Date(createdAt).getDate()} ${new Date(createdAt).getMonth()}` === date)
            .map(message => (<Comment.Group key={message.id}>
              <Comment>
                <Comment.Avatar src={message.avatar ? message.avatar : 'https://react.semantic-ui.com/images/wireframe/image.png'} />
                <Comment.Content>
                  <Comment.Author as='a'>{message.user}</Comment.Author>
                  <Comment.Metadata>
                    <div>{convertDate(message.createdAt)}</div>
                    {message.editedAt ? <div>{convertDate(message.editedAt)}</div> : null}
                  </Comment.Metadata>
                  <Comment.Text>{message.text}</Comment.Text>
                  {message.userId === userId ? <Comment.Actions>
                    <Comment.Action><EditMessage message={message} edit={edit}></EditMessage></Comment.Action>
                    <Comment.Action onClick={() => del(message.id)}>Delete</Comment.Action>
                  </Comment.Actions> : null}
                </Comment.Content>
              </Comment>
            </Comment.Group>))
        }
      </div>
    ))}
  </Container>)
};

export default MessageList;
