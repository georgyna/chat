import React from 'react';
import styles from './styles.module.scss';
import { IMessage } from '../../interfaces/message.interfaces';
import convertDate from '../../utils/convertDate';

export interface IMessageListProps {
    messages: IMessage[];
}

const Header = ({ messages }: IMessageListProps) => {
    const lastMessage = [...messages].pop();

    return (
        <div className={styles.headerWrapper}>
            <div className={styles.headerWrapperLeft}>
                <div className={styles.headerWrapperLeft__Label}>My Chat</div>
                <div className={styles.headerWrapperLeft__Label}>
                    {[...new Set(messages.map(({ user }) => user))].length} participants
            </div>
                <div className={styles.headerWrapperLeft__Label}>{messages.length} messages</div>
            </div>
            <div className={styles.headerWrapperRight}>
                last message at {lastMessage ? convertDate(lastMessage.createdAt) : null}
            </div>
        </div>);
};

export default Header;
