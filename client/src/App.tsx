import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Chat from './containers/Chat';
import ProtectedRoute from './containers/ProtectedRoute';
import Login from './containers/Login';
import MessageEditor from './containers/MessageEditor';
import UserEditor from './containers/UserEditor';
import UserList from './containers/UserList';
import { connect } from 'react-redux';

function App(props: any) {
  const isAdmin = false;
  console.log(props.user);

  return (
    <Switch>
        <Route path="/login" component={Login} />
        <ProtectedRoute path="/chat" component={Chat}></ProtectedRoute>
        <ProtectedRoute path="/message-editor" component={MessageEditor}></ProtectedRoute>
        {isAdmin ? (<ProtectedRoute path="/user-list" component={UserList}></ProtectedRoute>) : null}
        {isAdmin ? (<ProtectedRoute path="/user-editor" component={UserEditor}></ProtectedRoute>) : null}
			</Switch>
  );
}

const mapStateToProps = (rootState: any) => ({
  user: rootState.user
});

export default connect(mapStateToProps)(App);
