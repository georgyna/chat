import { createStore, applyMiddleware, combineReducers } from "redux";
import createSagaMiddleware from 'redux-saga';
import chatReducer from '../containers/Chat/reducer';
import loginReducer from '../containers/Login/reducer';
import rootSaga from '../sagas/index';

export default function configureStore() {
    const sagaMiddleware = createSagaMiddleware();
    const reducers = combineReducers({ chatReducer, loginReducer });
    const store = createStore(
        reducers,
        applyMiddleware(sagaMiddleware)
    );

    sagaMiddleware.run(rootSaga)

    return store;
}