const messagesApi = 'https://edikdolynskyi.github.io/react_sources/messages.json';

export const getMessages = async () => {
    const response = await fetch(messagesApi);
    return response.json();
};
